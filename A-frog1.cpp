#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define ull unsigned long long
#define mp make_pair
#define F first
#define S second
#define mod 1000000007

int a[100005];
int dp[100005];

int main()
{
   int n;
   cin >> n;
   for(int i=0;i<n;i++)
   {
       cin >> a[i];
   }

    dp[0] = 0;
    dp[1] = abs(a[1]-a[0]);

    for(int i=2;i<n;i++)
        dp[i] = min(dp[i-1] + abs(a[i]-a[i-1]), dp[i-2]+abs(a[i-2]-a[i]));
    
    cout << dp[n-1] << endl;
    return 0;


}
