#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define ull unsigned long long
#define mp make_pair
#define F first
#define S second
#define mod 1000000007

int h[100005];
int dp[100005];

int main()
{
    int n,k;
    cin >> n >> k;
    for(int i=0;i<n;i++)
    {
        cin >> h[i];
    }

    for(int i=1;i<n;i++)
    {
        int cur = INT_MAX;
        for(int j=1; j<k+1 && i-j >= 0 ;j++)
        {
            dp[i] = min(cur, dp[i-j] + abs(h[i] - h[i-j]) ); 
            cur = dp[i];
        }
    }

    cout << dp[n-1] << endl;

}
