#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define ull unsigned long long
#define mp make_pair
#define F first
#define S second
#define mod 1000000007


ll dp[100005];

int main()
{
    ll n,W;

    cin >> n >> W;

    ll wi,vi;
    for(int i=0;i<n;i++)
    {
        cin >>  wi >> vi;
        for(ll w_cur = W-wi; w_cur >= 0; w_cur--)
        {
            dp[w_cur + wi] = max(dp[w_cur + wi], dp[w_cur] + vi);
        }
    }

    ll ans = 0;
    for(auto l: dp)
        ans = max(ans, l);
    
    cout << ans << endl;

}
