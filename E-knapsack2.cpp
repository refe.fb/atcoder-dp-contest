#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define ull unsigned long long
#define mp make_pair
#define F first
#define S second
#define mod 1000000007

ll dp[100005];
ll weight[105];
ll value[105];

int main()
{
    ll n, W;
    cin >> n >> W;

    ll tot_value = 0;
    for(int i=0;i<n;i++)
    {
        cin >> weight[i] >> value[i];
        tot_value += value[i];
    }

    for(int i=0;i<100005;i++)
        dp[i] = 1e18L;

    dp[0] = 0;

    for(int i=0;i<n;i++)
    {
        for(ll cur_val = tot_value-value[i]; cur_val >= 0; cur_val--)
        {
            dp[cur_val + value[i]] = min(dp[cur_val + value[i]], dp[cur_val] + weight[i] );
        }
    }

    ll ans = 0;
    for(ll i=0; i<= tot_value ;i++)
    {
        if(W >= dp[i])
        {
            ans = max(ans, i);
        }
    }

    cout << ans << endl;

}
