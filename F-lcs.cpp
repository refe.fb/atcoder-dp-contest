#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define ull unsigned long long
#define mp make_pair
#define F first
#define S second
#define mod 1000000007

int dp[3005][3005];

// O(nm)

int main()
{
    string s,t;
    cin >> s >> t;


    for(int i=1; i<=s.size(); i++)
    {
        for(int j = 1;j<=t.size();j++)
        {
            if(s[i-1] == t[j-1])
            {
                dp[i][j] = max( max( dp[i-1][j-1] + 1, dp[i-1][j]), dp[i][j-1]);
            }
            else
            {
                dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
            }
        }
    }

    string ans = "";

    int i=s.size();
    int j=t.size();

    while(i>0 && j>0)
    {
        if(s[i-1] == t[j-1] && dp[i][j] == dp[i-1][j-1]+1)
        {
            ans += s[i-1];
            i--;
            j--;
        }
        else if( dp[i][j] == dp[i][j-1])
        {
            j--;
        }
        else
            i--;
    }

    reverse(ans.begin(), ans.end());
    cout << ans << endl;

}
