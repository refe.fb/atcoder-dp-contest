#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define ull unsigned long long
#define mp make_pair
#define F first
#define S second
#define mod 1000000007

int dist[100005];
vector<int> v[100005];
bool viz[100005];
int gelen[100005];

void dfs(int x)
{
    if(!viz[x] && gelen[x] == 0)
    {
        viz[x] = true;
        for(auto y: v[x])
        {
            gelen[y]--;
            dist[y] = max(dist[y] , dist[x]+1);
            if(gelen[y] == 0)
            {
                dfs(y);
            }
        }
    }
}


int main()
{
    int n,m;
    cin >> n >> m;

    for(int i=1;i<=n;i++)
    {
        v[0].pb(i);
        gelen[i]++;
    }

    int x,y;
    for(int i=0;i<m;i++)
    {
        cin >> x >> y;
        v[x].pb(y);
        gelen[y]++;
    }

    for(int i=0;i<=n;i++)
    {
        dfs(i);
    }

    int ans = -1;
    for(int i=0;i<=n;i++)
        ans = max(ans, dist[i]-1);
    cout << ans << endl;


}
