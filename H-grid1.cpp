#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define ull unsigned long long
#define mp make_pair
#define F first
#define S second
#define mod 1000000007

ll dp[1003][1005];
char a[1005][1004];

int main()
{
    int h,w;
    cin >> h >> w;

    for(int i=1; i<=h ;i++)
    {
        for(int j=1; j<=w; j++)
        {
            cin >> a[i][j];
        }
    }

    dp[1][0] = 1;
    for(int i=1;i<=h;i++)
    {
        for(int j=1;j<=w;j++)
        {
            if(a[i][j] == '.')
                dp[i][j] = (dp[i-1][j] + dp[i][j-1])%mod;
            //else
              //  dp[i][j] = 0;
        }
    }

    cout << dp[h][w] << endl;

}
