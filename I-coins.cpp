#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define ll long long
#define pii pair<int, int>
#define pll pair<long long, long long>
#define ull unsigned long long
#define mp make_pair
#define F first
#define S second
#define mod 1000000007

double dp[3005];

int main()
{
    int n;
    cin >> n;

    double p;
    dp[0] = 1;
    for(int i=1; i<=n; i++)
    {
        cin >> p;
        for(int j = i; j>=0; j--)
        {
            dp[j] = (j == 0 ? 0 : dp[j-1]*p) + dp[j]*(1-p); 
            
        }
    }

    //dp[j]: j tane tura gelme olasılığı
    double ans = 0;
    for(int i=0; i<=n; i++)
    {
        if(i > n-i)
        {
            ans += dp[i];
        }
    }

    printf("%.10lf\n", ans);
}
